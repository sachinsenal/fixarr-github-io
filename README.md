# Official site for FIXARR

Built using solidjs + unocss from scratch.  

# Contributing 

The docs ( markdown files ) are [here](https://github.com/FIXARR/fixarr.github.io/tree/src/src/routes/(index)/docs) , feel free to send PR's regarding spelling mistakes, incorrect grammar etc.

# Credits

- Thanks to [NvChad](https://github.com/NvChad)

# ⚠️ In Development
